class Post < ApplicationRecord
    validates :title, presence: true
    validates :description, length: {maximum: 140}, allow_blank: true
    validates :content, presence: true
    acts_as_votable
    has_one :category
end
