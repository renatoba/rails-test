class ApplicationController < ActionController::Base
    
    require "net/http"

    helper_method :current_user, :inspirational_quote

    def current_user
        if session[:user_id] and User.where(id: session[:user_id]).exists?
            @current_user ||= User.find(session[:user_id])
        else
            @current_user = nil
        end
    end

    # We just did this just to learn how to use files in ruby
    def inspirational_quote
        inspirational_quotes = File.new("app/assets/inspirational_quotes.txt", "r")
        number_of_quotes = File.read(inspirational_quotes).to_i
        selected_quote = rand(number_of_quotes)
        quote = IO.readlines(inspirational_quotes)[selected_quote]
        return quote
    end

end
