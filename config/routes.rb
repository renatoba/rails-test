Rails.application.routes.draw do

  get 'categories/new'
  get 'categories/delete'
  get 'categories/index'
  resources :users
  resources :posts do
    member do
      post 'upvote'
    end
  end


  resources :sessions, only: [:new, :create, :destroy]

  root "posts#index"

  get "/about" => "about_us#show"

  get "signup", to: "users#new", as: "signup"
  get "login", to: "sessions#new", as: "login"
  get "logout", to: "sessions#destroy", as: "logout"

  get "submit", to: "posts#new", as: "submit"
  post "submit-post", to: "posts#create", as: "submit-post"

end
